using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "New Data")]
public class DataLevels : ScriptableObject
{
    public List<Levels> listLevel = new List<Levels>();
}

[System.Serializable]
public class Levels
{
    public List<NodeFind> nodeFindList = new List<NodeFind>();
    public List<NodeFind> obstacles = new List<NodeFind>();
    public float time;
}

[System.Serializable]
public class NodeFind
{
    public CardID id;
    public int amount;
}