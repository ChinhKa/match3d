using UnityEngine;
using DG.Tweening;

public class DoScale_DT : MonoBehaviour
{
    public bool isLoop;
    public float delay;
    public float duration;

    void Start()
    {
        if (isLoop)
        {
            transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), duration).SetLoops(-1, LoopType.Yoyo).SetDelay(delay);
        }
        else
        {
            transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), duration).SetDelay(delay);
        }
    }
}
