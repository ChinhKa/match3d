using System.Collections;
using UnityEngine;
using UnityEngine.UI;
#if Firebase
using Firebase;
using Firebase.Firestore;
using Firebase.Extensions;
#endif
using System.Collections.Generic;
using TMPro;
using System.Threading.Tasks;
using System.Linq;
using DG.Tweening;
using System;
using System.Text.RegularExpressions;

public class UserManager : MonoBehaviour
{
    public static UserManager Ins;
    [HideInInspector]
    public bool online;

    [Header("USER")]
    public CanvasGroup userNameWelcomeGroup;
    public TextMeshProUGUI txtMess;
    public TextMeshProUGUI txtUserNameWelcome;

    [Header("UI")]
    public GameObject asyncLoadingUI;
    public GameObject registerUI;
    public GameObject rankUI;
    [Space]
    [Header("FIREBASE")]
#if Firebase
    public DependencyStatus dependencyStatus;
    [SerializeField] private FirebaseFirestore db;
#endif
    [Space]
    [Header("REGISTER ACCOUNT:")]
    [SerializeField] private TMP_InputField txtUserName;

    [Space]
    [Header("RANKER")]
    public int rankNbrs;
    public Ranker rankPre;
#if Firebase
    public List<User> user_List = new List<User>();
#endif
    private List<Ranker> rankerList = new List<Ranker>();
    public Transform rankGroup;
    public ScrollRect rankScroll;

    public List<Sprite> rankIcons = new List<Sprite>();

    public Ranker ranker_you;

    [Space]
    [Header("BUTTON:")]
    public Button btnRegisterUser;
 //public Button btnCloseRegisterUser;
    public Button btnCloseAsyncLoadingUI;
    private bool rankLoaded;
    public bool userLoaded;

    private void Awake()
    {
        if (Ins == null)
        {
            Ins = this;
        }
        try
        {
#if Firebase
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    InitializeFirebase();
                }
            });
#endif

        }
        catch (System.Exception ex)
        {
            Debug.Log("Loi " + ex);
        }
#if Firebase
        asyncLoadingUI.SetActive(false);
        InitRanks();
        HiddenMess();
#endif
    }

    private void Start()
    {
        btnRegisterUser.onClick.AddListener(() =>
        {
            SetUserName();
        });

      /*  btnCloseRegisterUser.onClick.AddListener(() =>
        {
            UIManager.ins.Active(UIManager.ins.home_UI);
        });*/

        if (PlayerPrefs.HasKey(GameConstants.USER_NAME.ToString()))
        {
            txtUserNameWelcome.text = Prefs.USER_NAME;
            userNameWelcomeGroup.alpha = 1;
            StartCoroutine(ShowUserNameWelcome());
        }
        else
        {
            userNameWelcomeGroup.alpha = 0;
        }

        btnCloseAsyncLoadingUI.onClick.AddListener(delegate { asyncLoadingUI.SetActive(false); });
    }

    private void InitializeFirebase()
    {
        try
        {
#if Firebase
            db = FirebaseFirestore.DefaultInstance;
#endif
        }
        catch (System.Exception exx)
        {

            Debug.Log("Looi" + exx);
        }
    }

    public void ShowRegisterForm()
    {
        registerUI.SetActive(true);
        rankUI.SetActive(false);
    }

    public void ShowRankUI()
    {
        asyncLoadingUI.SetActive(false);
        registerUI.SetActive(false);
        rankUI.SetActive(true);
        rankScroll.DOVerticalNormalizedPos(1, 0.5f);
    }

    private void SetUserName()
    {
        if (string.IsNullOrEmpty(txtUserName.text))
        {
            ShowMess("Username is empty!");
            Debug.Log("Username is empty!");
        }
        else
        {
            Debug.Log(txtUserName.text.Length);
            if (txtUserName.text.Length >= 4 && txtUserName.text.Length <= 20)
            {
                if (!Regex.IsMatch(txtUserName.text, @"^[a-zA-Z0-9_]+$"))
                {
                    ShowMess("The string does not contain special characters!");
                }
                else
                {
                    asyncLoadingUI.SetActive(true);
                    CheckUserExist();
                }
            }
            else
            {
                ShowMess("Invalid username!");
            }
        }
    }

    public void CheckUserExist()
    {
#if Firebase
        CollectionReference collection = db.Collection("users");
        Query query = collection.OrderByDescending("Rank");
        Task<QuerySnapshot> task = query.GetSnapshotAsync();
        task.ContinueWith(taskRS =>
        {
            if (taskRS.IsFaulted)
            {
                asyncLoadingUI.SetActive(false);
                ShowMess("Try again!");
            }
            else
            {
                user_List.Clear();
                List<DocumentSnapshot> documents = taskRS.Result.Documents.ToList();
                foreach (DocumentSnapshot obj in documents)
                {
                    User u = obj.ConvertTo<User>();
                    user_List.Add(u);
                }
                userLoaded = true;
            }
        });        
#endif
    }

    private void SaveUser()
    {
#if Firebase
        User userSave = new User
        {
            UserName = txtUserName.text,
            Rank = Prefs.LEVEL
        };

        db.Collection("users").Document(txtUserName.text).SetAsync(userSave).ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.LogError($"Failed to save user: {task.Exception}");
            }
            else
            {
                UIManager.ins.Active(UIManager.ins.rankUI);
                Debug.Log("User saved successfully!");
            }
        });
        rankLoaded = true;
        Prefs.FIRST_TIME_PLAY = 1;
        Prefs.USER_NAME = txtUserName.text;
        Debug.Log("Completed!");
#endif
    }

    public void UpdateRank()
    {
#if Firebase
        Dictionary<string, object> userUpdate = null;

        userUpdate = new Dictionary<string, object>
            {
                {"Rank", Prefs.LEVEL}
            };

        DocumentReference reference = db.Collection("users").Document(Prefs.USER_NAME);
        reference.UpdateAsync(userUpdate).ContinueWithOnMainThread(task =>
        {
            Debug.Log("Updated");
        });
#endif
    }

    public void ShowRank()
    {
#if Firebase
        if(!rankLoaded)
            asyncLoadingUI.SetActive(true);
        
        CollectionReference collection = db.Collection("users");
        Query query = collection.OrderByDescending("Rank");
        Task<QuerySnapshot> task = query.GetSnapshotAsync();
        task.ContinueWith(taskRS =>
        {
            if (taskRS.IsFaulted)
            {
                Debug.Log("Err");
            }
            else
            {
                user_List.Clear();
                List<DocumentSnapshot> documents = taskRS.Result.Documents.ToList();
                foreach (DocumentSnapshot obj in documents)
                {
                    User u = obj.ConvertTo<User>();
                    user_List.Add(u);
                }
                rankLoaded = true;
            }
        });
#endif
    }

    private void Update()
    {
#if Firebase

        if (userLoaded)
        {
            bool check = user_List.Find(u => u.UserName == txtUserName.text).UserName == null ? false : true;
            if (check)
            {
                asyncLoadingUI.SetActive(false);
                ShowMess("Username existed!");
            }
            else
            {
                SaveUser();
            }
            userLoaded = false;
        }

        if (rankLoaded)
        {
            ShowRankUI();
            asyncLoadingUI.SetActive(false);
            UIManager.ins.Active(UIManager.ins.rankUI);
            for (int i = 0; i < user_List.Count; i++)
            {
                rankerList[i].gameObject.SetActive(true);
                if (i <= 2)
                {
                    if (i == 0)
                    {
                        rankerList[i].icon.GetComponent<Image>().sprite = rankIcons[0];
                    }
                    else if (i == 1)
                    {
                        rankerList[i].icon.GetComponent<Image>().sprite = rankIcons[1];
                    }
                    else
                    {
                        rankerList[i].icon.GetComponent<Image>().sprite = rankIcons[2];
                    }

                    rankerList[i].ShowRankFrame(true);
                    rankerList[i].SetInfo("", user_List[i].UserName, user_List[i].Rank.ToString());
                }
                else
                {
                    if(user_List[i].UserName == Prefs.USER_NAME)
                    {
                        ranker_you.SetInfo((i + 1).ToString(), user_List[i].UserName, Prefs.LEVEL.ToString());
                    }

                    rankerList[i].SetInfo((i + 1).ToString(), user_List[i].UserName, user_List[i].Rank.ToString());
                    rankerList[i].ShowRankFrame(false);
                }

            }
            rankLoaded = false;
        }
#endif
    }

    private void InitRanks()
    {
        for (int i = 0; i < rankNbrs; i++)
        {
            Ranker ranker = Instantiate(rankPre);
            rankerList.Add(ranker);
            ranker.transform.SetParent(rankGroup);
            ranker.transform.localRotation = Quaternion.identity;
            ranker.transform.localPosition = Vector3.zero;
            ranker.transform.localScale = Vector3.one;
        }
    }

    private void ShowMess(string mss)
    {
        txtMess.gameObject.SetActive(true);
        txtMess.text = mss;
    }

    private void HiddenMess()
    {
        txtMess.gameObject.SetActive(false);
        txtMess.text = "";
    }

    private IEnumerator ShowUserNameWelcome()
    {
        yield return new WaitForSeconds(3);
        userNameWelcomeGroup.DOFade(0, 1);
    }
}