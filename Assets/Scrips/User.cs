using Firebase.Firestore;
[FirestoreData]
public struct User
{
    [FirestoreProperty]
    public string UserName { get; set; }
    [FirestoreProperty]
    public int Rank { get; set; }
}
