using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public static AudioManager ins;

    [Header("AUDIO SOURCE:")]
    [SerializeField] private AudioSource audioSource_BGSound;
    [SerializeField] private AudioSource audioSource_VFXSound;

    [Header("AUDIO CLIP:")]
    public AudioClip BGClip;
    public AudioClip winClip;
    public AudioClip loseClip;
    public AudioClip clickClip;
    public AudioClip matchClip;
    public AudioClip noMatchClip;
    public AudioClip frozenClip;
    public AudioClip fanClip;
    public AudioClip autoFindClip;
    public AudioClip magnetClip;
    public AudioClip thunderClip;
    public AudioClip touchClip;
    public AudioClip buyClip;

    public void Awake()
    {
        if (ins == null)
            ins = this;
    }
    public void Start()
    {
        if (!PlayerPrefs.HasKey(GameConstants.BG_MUSIC))
        {
            Prefs.BG_MUSIC = 1;
        }
        if (!PlayerPrefs.HasKey(GameConstants.VFX_MUSIC))
        {
            Prefs.VFX_MUSIC = 1;
        }
        PlayBGSound();

    }
    
    public void PlayTouchSound() => audioSource_VFXSound.PlayOneShot(touchClip);
    public void PlayLoseSound()
    {
        audioSource_BGSound.Stop();
        audioSource_VFXSound.PlayOneShot(loseClip);
    }
    public void PlayWonSound()
    {
        audioSource_BGSound.Stop();
        audioSource_VFXSound.PlayOneShot(winClip);
    }
    public void PlayBGSound() => audioSource_BGSound.Play();
    public void PlayEatSound() => audioSource_VFXSound.PlayOneShot(matchClip);
    public void PlayBuySound() => audioSource_VFXSound.PlayOneShot(buyClip);
    public void PlayClickSound() => audioSource_VFXSound.PlayOneShot(clickClip);
    public void PlayNoMatchSound() => audioSource_VFXSound.PlayOneShot(noMatchClip);
    public void PlayFanSound() => audioSource_VFXSound.PlayOneShot(fanClip);
    public void PlayAutoFindSound() => audioSource_VFXSound.PlayOneShot(autoFindClip);
    public void PlayMagnetSound() => audioSource_VFXSound.PlayOneShot(magnetClip);
    public void PlayThunderSound() => audioSource_VFXSound.PlayOneShot(thunderClip);
    public void PlayFrozenTime() => audioSource_VFXSound.PlayOneShot(frozenClip);
}
