using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    [HideInInspector] public CardID id;
    [HideInInspector] public int amount;
    public TextMeshProUGUI txtAmount;
    public Image statusIMG;
    public Image icon;

    public void Setter(CardID id, Sprite icon, int amount)
    {
        this.id = id;
        this.icon.sprite = icon;
        this.amount = amount;
        this.txtAmount.text = amount.ToString();
        statusIMG.enabled = false;

        this.icon.SetNativeSize();
    }
}
