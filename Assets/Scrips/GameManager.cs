using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager ins;
    [HideInInspector] public GameState gameState;
    [HideInInspector] public List<Node> findNodesList = new List<Node>();
    [HideInInspector] public List<Node> obstaclesNodesList = new List<Node>();
    private List<Transform> listNodeInQueue = new List<Transform>();
    private float countDown_Time;
    [HideInInspector] public bool isFrozenTime;
    [HideInInspector] public bool isThunder;
    [HideInInspector] public bool isPowerfullFan;
    [HideInInspector] public bool isBonusTime;
    private int count_Magnet;
    private CardID nodeID_magnet;
    public Transform posSpaw;
    private int quantity_Node;
    public Transform gate;
    public Animator gateAnimator;
    public ParticleSystem gateEffect_Special;
    public ParticleSystem gateEffect_Normal;
    public ParticleSystem findEffect;
    public ParticleSystem notMatchEffect;
    public ParticleSystem thunderEffect;
    [Header("WALL:")]
    public Transform leftWall;
    public Transform rightWall;

    [HideInInspector] public int levelIdx = 0;

    private void Awake()
    {
        if (ins == null)
            ins = this;
    }

    private void Start()
    {
        WallScaler();
        gameState = GameState.pause; 
    }

    private void Update()
    {
        if (GameEvent.ins.gameStarted.Value && GameEvent.ins.gameReady.Value)
        {
            TimeLine_Processing();

            if (UIManager.ins.fanSlider.value < UIManager.ins.fanSlider.maxValue)
            {
                UIManager.ins.fanSlider.value += Time.deltaTime;
            }
        }
    }

    public void BoostersProcessing()
    {
        if (isThunder)
        {
            Thunder();
            Prefs.THUNDER_ITEM--;
            isThunder = false;
        }

        if (isPowerfullFan)
        {
            Prefs.POWERFULL_FAN--;
            UIManager.ins.fanSlider.value = UIManager.ins.fanSlider.maxValue;
            isPowerfullFan = false;
        }

        if (isBonusTime)
        {
            Prefs.BONUS_TIME_ITEM--;
            countDown_Time += 30;
            isBonusTime = false;
        }
    }

    private void TimeLine_Processing()
    {
        if (!GameEvent.ins.gameWon.Value && !GameEvent.ins.gameLost.Value)
        {
            if (countDown_Time <= 0)
            {
                GameEvent.ins.gameLost.Value = true;
                UIManager.ins.txtTime.text = "Time up!";
                countDown_Time = 1;
            }
            else
            {
                if (!isFrozenTime)
                {
                    countDown_Time -= Time.deltaTime;
                }

                UIManager.ins.txtTime.text = Mathf.Floor(countDown_Time / 60).ToString("00") + ":" + (countDown_Time % 60).ToString("00");
            }
        }
    }

    public void Frozen(Action<bool> callback)
    {
        if (Prefs.FROZEN_TIME_ITEM > 0)
        {
            StartCoroutine(FrozenHandle());
            Prefs.FROZEN_TIME_ITEM--;
            callback(true);
        }
        else
        {
            callback(false);
        }
    }

    private IEnumerator FrozenHandle()
    {
        UIManager.ins.bgFrozen.alpha = 0;
        UIManager.ins.bgFrozen.DOFade(1, 3f).SetLoops(-1, LoopType.Yoyo);
        isFrozenTime = true;
        yield return new WaitForSeconds(10);
        UIManager.ins.bgFrozen.alpha = 0;
        DOTween.Kill(UIManager.ins.bgFrozen);
        isFrozenTime = false;
    }

    public void Thunder() =>
            StartCoroutine(ThunderProcessing());

    private IEnumerator ThunderProcessing()
    {
        int i = 0;
        while (i <= 2)
        {
            thunderEffect.transform.position = obstaclesNodesList[i].transform.position;
            thunderEffect.Play();
            yield return new WaitForSeconds(0.2f);
            Destroy(obstaclesNodesList[i].gameObject);
            obstaclesNodesList.Remove(obstaclesNodesList[i]);
            i++;
            yield return new WaitForSeconds(0.2f);
        }
    }

    void WallScaler()
    {
        float screenWidth = Camera.main.orthographicSize * 2 * Camera.main.aspect;
        float leftWidth = leftWall.localScale.x;
        float rightWidth = rightWall.localScale.x;
        float xPositionLeft = -screenWidth / 2 + leftWidth / 2;
        float xPositionRight = screenWidth / 2 - rightWidth / 2;
        leftWall.position = new Vector3(xPositionLeft, leftWall.position.y, leftWall.position.z) + new Vector3(3.7f, 0, 0);
        rightWall.position = new Vector3(xPositionRight, rightWall.position.y, rightWall.position.z) + new Vector3(3.9f, 0, 0);
    }

    public void UseMagnet(Action<bool> callback)
    {
        if (findNodesList.Count != 0 && Prefs.MAGNET_ITEM > 0)
        {
            Prefs.MAGNET_ITEM--;
            callback(true);
            gateEffect_Special.Play();
            findNodesList[0].transform.DOMove(new Vector3(findNodesList[0].transform.position.x, 4, findNodesList[0].transform.position.z), 1f).OnComplete(() =>
            {
                findNodesList[0].transform.DOMove(new Vector3(gate.position.x, 4, gate.position.z), 0.5f).OnComplete(() =>
                {
                    findNodesList[0].ValidProcessing((x) =>
                    {
                        if (x)
                        {
                            gateEffect_Special.Stop();
                        }
                    });
                });
            });
        }
        else
        {
            callback(false);
        }
    }

    public void UseFindItem(Action<bool> callback)
    {
        if (findNodesList.Count != 0 && Prefs.FIND_ITEM > 0)
        {
            Prefs.FIND_ITEM--;
            callback(true);
            foreach (var item in findNodesList)
            {
                if (item.transform.childCount == 1)
                {
                    ParticleSystem pre = Instantiate(findEffect, item.transform.position, Quaternion.identity);
                    pre.transform.SetParent(item.transform);
                    pre.Play();
                    break;
                }
            }
        }
        else
        {
            callback(false);
        }
    }

    public void UseFindItems(Action<bool> callback)
    {
        if (findNodesList.Count != 0 && Prefs.FIND_ITEMS > 0)
        {
            Prefs.FIND_ITEMS--;
            callback(true);
            int i = 0;
            foreach (var item in findNodesList)
            {
                if (i <= 2)
                {
                    if (item.transform.childCount == 1)
                    {
                        ParticleSystem pre = Instantiate(findEffect, item.transform.position, Quaternion.identity);
                        pre.transform.SetParent(item.transform);
                        pre.Play();
                        i++;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            callback(false);
        }
    }

    public void UseFan(Action<bool> callback)
    {
        if (UIManager.ins.fanSlider.value >= UIManager.ins.fanSlider.maxValue)
        {
            callback(true);
            if (findNodesList.Count != 0)
            {
                foreach (var f in findNodesList)
                {
                    f.Push();
                }
            }

            if (obstaclesNodesList.Count != 0)
            {
                foreach (var o in obstaclesNodesList)
                {
                    o.Push();
                }
            }
            UIManager.ins.fanSlider.value = 0;
        }
        else
        {
            callback(false);
        }
    }

    public void LoadLevel()
    {
        if (findNodesList.Count != 0)
        {
            foreach (var o in findNodesList)
            {
                if (o != null)
                {
                    Destroy(o.gameObject);
                }
            }
        }
        if (obstaclesNodesList.Count != 0)
        {
            foreach (var o in obstaclesNodesList)
            {
                if (o != null)
                {
                    Destroy(o.gameObject);
                }
            }
        }
        UIManager.ins.fanSlider.value = 0;
        UIManager.ins.ClearFindCard();
        findNodesList.Clear();
        obstaclesNodesList.Clear();
        GameEvent.ins.ResetEvent();
        ResetPartical();
        DataLevelProcessing();
        Time.timeScale = 1;
    }

    private void ResetPartical()
    {
        findEffect.Stop();
        notMatchEffect.Stop();
        thunderEffect.Stop();
        gateEffect_Normal.Stop();
        gateEffect_Special.Stop();
    }

    public void AddCoin(int value)
    {
        ManagerAds.Ins.ShowRewardedVideo((x) =>
        {
            if (x)
            {
                Prefs.COIN += value;
                UIManager.ins.ShowCoin();
            }
        });
    }

    public void CloseGate()
    {
        gateEffect_Normal.Stop();
        gateAnimator.ResetTrigger("Open");
        gateAnimator.SetTrigger("Close");
    }

    public void OpenGate()
    {
        gateEffect_Normal.Play();
        gateAnimator.ResetTrigger("Close");
        gateAnimator.SetTrigger("Open");
    }

    public void BackHome()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ZoomInOut_DW(Transform tr)
    {
        tr.localScale = Vector3.zero;
        tr.DOScale(Vector3.one, 0.2f).SetUpdate(true);
    }

    public void DataLevelProcessing()
    {
        levelIdx = Prefs.LEVEL < DataManager.ins.data.dataLevels.listLevel.Count - 1 ? Prefs.LEVEL : UnityEngine.Random.Range(1, DataManager.ins.data.dataLevels.listLevel.Count);
        Time.timeScale = 1;
        UIManager.ins.ShowLevel();
        UIManager.ins.RenderFindCard();
        countDown_Time = DataManager.ins.data.dataLevels.listLevel[levelIdx].time;
        StartCoroutine(GenerateNode());
    }

    public void NextTest()
    {
        Prefs.LEVEL++;
        LoadLevel();
    }

    private IEnumerator GenerateNode()
    {
        List<Transform> listTemp = new List<Transform>();
        foreach (var o in DataManager.ins.data.dataLevels.listLevel[levelIdx].nodeFindList)
        {
            for (int i = 0; i < o.amount; i++)
            {
                CardInfo pre = DataManager.ins.data.dataCard.listCard.Find(c => c.id == o.id);
                if (pre != null)
                {
                    Transform tr = Instantiate(pre.pre, posSpaw.position, Quaternion.identity);
                    tr.GetComponent<Node>().SetNodeID(pre.id);
                    tr.transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 180));
                    quantity_Node++;
                    findNodesList.Add(tr.GetComponent<Node>());
                    tr.gameObject.SetActive(false);
                    listTemp.Add(tr);
                }
            }
        }

        foreach (var o in DataManager.ins.data.dataLevels.listLevel[levelIdx].obstacles)
        {
            for (int i = 0; i < o.amount; i++)
            {
                CardInfo pre = DataManager.ins.data.dataCard.listCard.Find(c => c.id == o.id);
                if (pre != null)
                {
                    Transform tr = Instantiate(pre.pre, posSpaw.position, Quaternion.identity);
                    tr.GetComponent<Node>().SetNodeID(pre.id);
                    tr.transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 180));
                    obstaclesNodesList.Add(tr.GetComponent<Node>());
                    tr.gameObject.SetActive(false);
                    listTemp.Add(tr);
                }
            }
        }

        listTemp.Shuffle();

        foreach (var o in listTemp)
        {
            o.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.05f);
        }

        UIManager.ins.ShowBoostersUI();
    }

    public void CheckFindCard(CardID iD)
    {
        Card card = UIManager.ins.listFindCard.Find(c => c.id == iD);
        if (card != null)
        {
            card.amount--;
            card.txtAmount.text = card.amount.ToString();
            if (card.amount == 0)
            {
                UIManager.ins.listFindCard.Remove(card);
                CheckWin();
                card.statusIMG.enabled = true;
                card.transform.DOLocalRotate(new Vector3(0, 180, 0), 0.5f).OnComplete(() =>
                {
                    Destroy(card.gameObject);
                });
            }
        }
    }

    public void CheckWin()
    {
        if (UIManager.ins.listFindCard.Count == 0)
        {
            GameEvent.ins.gameWon.Value = true;
        }
    }

    public bool CheckCardValid(CardID iD)
    {
        Card card = UIManager.ins.listFindCard.Find(C => C.id == iD);
        if (card == null)
        {
            return false;
        }
        return true;
    }
}
