using System.Collections;
using UnityEngine;
using UniRx;

public class GameEvent : MonoBehaviour
{
    public static GameEvent ins;

    private void Awake()
    {
        if (ins == null)
            ins = this;
    }
    private CompositeDisposable subscriptions = new CompositeDisposable();
    public BoolReactiveProperty gameStarted { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty gameReady { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty gameWon { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty gameLost { get; set; } = new BoolReactiveProperty(false);

    private void OnEnable()
    {
        StartCoroutine(Subscribe());
    }
    private IEnumerator Subscribe()
    {
        yield return new WaitUntil(() => ins != null);

        gameReady.ObserveEveryValueChanged(x => x.Value)
            .Subscribe(value =>
            {
                if (value)
                {
                    GameManager.ins.gameState = GameState.playing;
                }
            })
            .AddTo(subscriptions);

        gameLost.ObserveEveryValueChanged(x => x.Value)
            .Subscribe(value =>
            {
                if (value)
                {
                    GameManager.ins.gameState = GameState.pause;
                    Prefs.WINNING_STREAK = 0;
                    AudioManager.ins.PlayLoseSound();
                    GameManager.ins.ZoomInOut_DW(UIManager.ins.gameOver_UI.transform.GetChild(0));
                    UIManager.ins.gameOver_UI.SetActive(true);
                }
            })
            .AddTo(subscriptions);

        gameWon.ObserveEveryValueChanged(x => x.Value)
            .Subscribe(value =>
            {
                if (value)
                {
                    GameManager.ins.gameState = GameState.pause;
                    if (PlayerPrefs.HasKey(GameConstants.WINNING_STREAK))
                    {
                        Prefs.WINNING_STREAK = Prefs.WINNING_STREAK >= 3 ? 1 : (Prefs.WINNING_STREAK + 1);

                        switch (Prefs.WINNING_STREAK)
                        {
                            case 1:
                                Prefs.THUNDER_ITEM += 1;
                                Prefs.BONUS_TIME_ITEM += 1;
                                Prefs.POWERFULL_FAN += 1;
                                break;
                            case 2:
                                Prefs.THUNDER_ITEM += 2;
                                Prefs.BONUS_TIME_ITEM += 2;
                                Prefs.POWERFULL_FAN += 2;
                                break;
                            case 3:
                                Prefs.THUNDER_ITEM += 3;
                                Prefs.BONUS_TIME_ITEM += 3;
                                Prefs.POWERFULL_FAN += 3;
                                break;
                        }
                    }

                    Prefs.LEVEL_CHEST_PROGRESS++;
                    Prefs.LEVEL++;
                    if (PlayerPrefs.HasKey(GameConstants.USER_NAME))
                    {
                        UserManager.Ins.UpdateRank();
                    }
                    GameManager.ins.ZoomInOut_DW(UIManager.ins.winGame_UI.transform.GetChild(0));
                    UIManager.ins.winGame_UI.SetActive(true);
                    AudioManager.ins.PlayWonSound();
                }
            })
            .AddTo(subscriptions);
    }
    private void OnDisable()
    {
        subscriptions.Clear();
    }
    public void ResetEvent()
    {
        gameReady.Value = false;
        gameWon.Value = false;
        gameLost.Value = false;
        GameManager.ins.gameState = GameState.pause;
    }
}
