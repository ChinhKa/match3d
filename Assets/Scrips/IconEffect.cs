using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class IconEffect : MonoBehaviour
{
    public Image icon;
    public TextMeshProUGUI txtAmount;

    public void SetInfo(Sprite icon, string txtAmount, Vector3 pos)
    {
        this.icon.sprite = icon;
        this.txtAmount.text = txtAmount;
        transform.position = pos;
        transform.SetParent(UIManager.ins.canvas);
        transform.localScale = Vector3.one;
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));

        transform.DOLocalMoveY(-400, 1f).SetEase(Ease.InOutBack).OnComplete(() =>
        {
            Destroy(gameObject);
        });
    }
}
