using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class LuncherInfo : MonoBehaviour
{
    public CardID iD;
    public int quantity;
    public TextMeshProUGUI txtQuantity;
    public bool isFull;
    public Image icon;
    public Animator animator;
    public void SetInfo(CardID id, int quan, Sprite ic)
    {
        iD = id;
        quantity = quan;
        txtQuantity.text = "X" + quan;
        icon.sprite = ic;
    }

    public void UpdateInfo(int sl)
    {
        quantity -= sl;
        if (quantity <= 0)
        {
            isFull = true;
            txtQuantity.text = "Full";
            animator.SetTrigger("Close");
        }
        else
        {
            txtQuantity.text = "X" + quantity;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ActiveZone"))
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }

        if (collision.gameObject.CompareTag("HiddenZone"))
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
