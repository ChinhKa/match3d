using UnityEngine;
using UnityEngine.UI;

public class DailyReward : MonoBehaviour
{
    public IDItem iD;
    public Button btn;
    public Image status;
    public bool enableClick;

    private void Start()
    {
        btn.onClick.AddListener(() =>
        {
            if (enableClick && !status.gameObject.activeInHierarchy)
            {
                switch (iD)
                {
                    case IDItem.magnet:
                        Prefs.MAGNET_ITEM++;
                        break;
                    case IDItem.findItem:
                        Prefs.FIND_ITEM++;
                        break;
                    case IDItem.findItems:
                        Prefs.FIND_ITEMS++;
                        break;
                    case IDItem.bonusItem:
                        Prefs.BONUS_TIME_ITEM++;
                        break;
                    case IDItem.fanItem:
                        Prefs.POWERFULL_FAN++;
                        break;
                    case IDItem.frozenItem:
                        Prefs.FROZEN_TIME_ITEM++;
                        break;
                    case IDItem.thunderItem:
                        Prefs.THUNDER_ITEM++;
                        break;
                    default:
                        break;
                }
                enableClick = false;
                status.gameObject.SetActive(true);
                Prefs.RECEIVED_DAILY_REWARDS = 1;
                Prefs.DAILY_REWARDS++;
            }
            AudioManager.ins.PlayClickSound();
        });
    }
}


