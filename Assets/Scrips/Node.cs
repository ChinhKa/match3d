﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;

public class Node : MonoBehaviour
{
    public CardID id;
    [HideInInspector] public bool isSelected;
    [HideInInspector] public bool isValid;
    private Vector3 screenPoint;
    private Vector3 offset;
    private float coolDownReset = 0.5f;
    private Vector3 initialPosition;
    private Rigidbody rb;
    public float forceMultiplier = 10.0f;
    private bool collisionGate;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    public CardID GetNodeID() => id;
    public void SetNodeID(CardID id) => this.id = id;

    void OnMouseDown()
    {
        if (GameManager.ins.gameState == GameState.playing)
        {   
            initialPosition = transform.position;
            AudioManager.ins.PlayTouchSound();
            offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            coolDownReset = 0.5f;
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
            transform.rotation = Quaternion.Euler(new Vector3(-100, 0, 180));
        }
    }

    void OnMouseDrag()
    {
        if (GameManager.ins.gameState == GameState.playing)
        {
            if(!isReadyUpdatePos)
                StartCoroutine(UpdatePosition());
            GetComponent<Collider>().isTrigger = true;
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
            curPosition.y = 8;
            transform.position = curPosition;
        }
    }

    bool isReadyUpdatePos;
    IEnumerator UpdatePosition() {
        isReadyUpdatePos = true;
        yield return new WaitForSeconds(0.03f);
        initialPosition = transform.position;
        isReadyUpdatePos = false;
    }

    private void OnMouseUp()
    {
        if (GameManager.ins.gameState == GameState.playing)
        {
            GetComponent<Collider>().isTrigger = false;
            Vector3 direction = transform.position - initialPosition;
            direction.y = 0;

            if (Physics.Raycast(transform.position, direction, 200, LayerMask.GetMask("Gate")))
            {
                transform.DOMove(GameManager.ins.gate.position + new Vector3(0, 4, 0), 0.5f).OnComplete(() =>
                {
                    isValid = GameManager.ins.CheckCardValid(id) ? true : false;
                    if (isValid)
                    {
                        ValidProcessing();
                    }
                    else
                    {
                        AudioManager.ins.PlayNoMatchSound();
                        GameManager.ins.notMatchEffect.Play();
                        rb.AddForce(new Vector3(0, 3, 10) * forceMultiplier, ForceMode.Impulse);
                    }
                });

                return;
            }

            rb.AddForce(direction * forceMultiplier, ForceMode.Impulse);             

            if (isValid)
            {
                ValidProcessing();
            }
            else
            {
                if (collisionGate)
                {
                    AudioManager.ins.PlayNoMatchSound();
                    GameManager.ins.notMatchEffect.Play();
                    rb.AddForce(new Vector3(0, 3, 10) * forceMultiplier, ForceMode.Impulse);
                }
            }
        }
    }

    public void ValidProcessing(Action<bool> callback = null)
    {
        GameManager.ins.OpenGate();
        AudioManager.ins.PlayEatSound();
        transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 180));
        transform.position = GameManager.ins.gate.position;
        GetComponent<Collider>().enabled = false;
        rb.isKinematic = true;
        transform.DOScale(Vector3.zero, 0.5f).SetDelay(0.5f).OnComplete(() =>
        {
            if (callback != null)
            {
                callback(true);
            }
            GameManager.ins.findNodesList.Remove(this);
            GameManager.ins.CheckFindCard(id);
            GameManager.ins.CloseGate();
            Destroy(gameObject);
        });
        this.enabled = false;
    }

    private void OnMouseEnter()
    {
        if (GameManager.ins.gameState == GameState.playing)
        {
            isSelected = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("SelectZone"))
        {
            isValid = GameManager.ins.CheckCardValid(id) ? true : false;
            collisionGate = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("SelectZone"))
        {
            rb.isKinematic = false;
            isValid = false;
            collisionGate = false;
        }
    }

    public IEnumerator DelayRemoveNode()
    {
        yield return new WaitForSeconds(0.01f);
        if (!isValid)
        {
            transform.DOMove(new Vector3(10, 0, 0), 0.2f).OnComplete(() =>
            {
                Destroy(gameObject);
            });
        }
    }

    public void Push()
    {
        if (rb != null)
            rb.AddForce(new Vector3(0, 0, 200), ForceMode.Impulse);
    }
}