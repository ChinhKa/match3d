using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.UI;

public class SettingPopup : MonoBehaviour
{
    [SerializeField] private Button btnQuit;
    [SerializeField] private Button btnRateUs;
    [SerializeField] private Button btnMusic;
    [SerializeField] private Button btnSound;
    [SerializeField] private Button btnNotify;
    [SerializeField] private Sprite musicOn;
    [SerializeField] private Sprite musicOff;
    [SerializeField] private Sprite soundOn;
    [SerializeField] private Sprite soundOff;
    [SerializeField] private Sprite notifyOn;
    [SerializeField] private Sprite notifyOff;

    private void Start()
    {
        ActiveButton();

        btnRateUs.onClick.AddListener(() =>
        {
            Application.OpenURL("https://play.google.com/store/games?hl=en-VN");
        });

        btnQuit.onClick.AddListener(() =>
        {
            Application.Quit();
        });

        btnSound.onClick.AddListener(() =>
        {
            if (Prefs.VFX_MUSIC == 0)
                Prefs.VFX_MUSIC = 1;
            else
                Prefs.VFX_MUSIC = 0;

            ActiveButton();
        });

        btnMusic.onClick.AddListener(() =>
        {
            if (Prefs.BG_MUSIC == 0)
                Prefs.BG_MUSIC = 1;
            else
                Prefs.BG_MUSIC = 0;
            ActiveButton();
        });

        btnNotify.onClick.AddListener(() =>
        {
            if (Prefs.NOTIFY == 0)
                Prefs.NOTIFY = 1;
            else
                Prefs.NOTIFY = 0;
            ActiveButton();
        });
    }

    private void ActiveButton() { 
        btnMusic.transform.GetChild(0).GetComponent<Image>().sprite = Prefs.BG_MUSIC == 0 ? musicOn : musicOff;
        btnMusic.transform.GetChild(0).GetComponent<Image>().SetNativeSize();
        btnSound.transform.GetChild(0).GetComponent<Image>().sprite = Prefs.VFX_MUSIC == 0 ? soundOn : soundOff;
        btnSound.transform.GetChild(0).GetComponent<Image>().SetNativeSize();
        btnNotify.transform.GetChild(0).GetComponent<Image>().sprite = Prefs.NOTIFY == 0 ? notifyOn : notifyOff;
        btnNotify.transform.GetChild(0).GetComponent<Image>().SetNativeSize();
    }
}
