﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;

    [Space(5)]
    [Header("TEXT:")]
    public TextMeshProUGUI txtLevel;
    public TextMeshProUGUI txtLevel2;
    public TextMeshProUGUI txtCoin_HomeUI;
    public TextMeshProUGUI txtTime;
    public TextMeshProUGUI txtLevelInSelectBoostersUI;

    public TextMeshProUGUI txtDailyRewardTime;
    static TimeSpan giftCooldown_DailyReward = new TimeSpan(0, 59, 59);
    static TimeSpan timeSinceLastGift_DailyReward;
    static long lastGiftTimeTicks_DailyReward;
    static DateTime lastGiftTime_DailyReward;
    static DateTime giftCooldown_SpeialOffer;


    [Header("STREAK:")]
    public TextMeshProUGUI txtThunderNbr;
    public TextMeshProUGUI txtBonusTimeNbr;
    public TextMeshProUGUI txtPowerfullFanNbr;
    public TextMeshProUGUI txtLevelChestProgressNbr;

    public TextMeshProUGUI txtFindNbr;
    public TextMeshProUGUI txtFindsNbr;
    public TextMeshProUGUI txtMagnetNbr;
    public TextMeshProUGUI txtFrozenNbr;

    [Space(5)]
    [Header("UI:")]
    public GameObject menuUI;
    public GameObject home_UI;
    public GameObject gamePlay_UI;
    public GameObject gameOver_UI;
    public GameObject winGame_UI;
    public GameObject EndGame_UI;
    public GameObject setting_UI;
    public GameObject pause_UI;
    public GameObject mess_UI;
    public GameObject shop_UI;
    public GameObject commingSoonUI;
    public GameObject rankUI;
    public GameObject limitedOfferUI;
    public GameObject selectBoostersUI;
    public GameObject ResetBoostersMessUI;
    public GameObject dailyRewardUI;
    public GameObject levelChessUI;
    public GameObject giftSpeicalUI;

    [Space(5)]
    [Header("BUTTON:")]
    public Button btnPlay;
    public Button btnPlay_Accept;
    public Button btnCloseSelectBoosters;
    public Button btnNext;
    public Button btnRetry;
    public Button btnST;
    public Button btnPause;
    public Button btnCloseST;
    public Button btnRetry_PauseUI;
    public Button btnRetry_LoseUI;
    public Button btnHome_WinUI;
    public Button btnRetry_GameOverUI;
    public Button btnLimitedOffer;
    public Button btnCloseLimitedOffer;
    public Button btnSubmitQuitLevel;
    public Button btnCloseResetBoostersMessUI;
    public Button btnDailyReward;
    public Button btnCloseDailyReward;
    public Button btnLevelChest;
    public Button btnCloseLevelChest;

    [Space(5)]
    [Header("SLIDER:")]
    public Slider fanSlider;
    public Slider levelRewardProgression;
    public Slider levelChestSlider;

    [Header("BOOSTER ITEMS:")]
    public Button btnThunderItem;
    public Button btnBonusTimeItem;
    public Button btnPowerfullFanItem;

    [Header("MENU:")]
    public Button btnShop;
    public Button btnLives;
    public Button btnHome;
    public Button btnTeam;
    public Button btnRank;
    public Button btnClosePauseUI;
    public Button btnBonusCoin_HomeUI;
    public Button btnBonusCoin_ShopUI;
    public Button btnClose_BonusCoin_ShopUI;

    public Button btnST_WinUI;
    public Button btnST_PauseUI;
    public Button btnBackHome_EndUI;
    public Button btnResumePauseUI;

    public Button btnFrozenTime;
    public Button btnMagnet;
    public Button btnFan;
    public Button btnFindItem;
    public Button btnFindItems;

    public Button btnCollectGiftSpecial;
    public Button btnCloseGiftSpecialUI;

    public TextMeshProUGUI txtNbrsMagnetItems;
    public TextMeshProUGUI txtNbrsFrozenTimeItems;

    public Transform cardPre;
    public RectTransform findCardList;

    public List<Card> listFindCard = new List<Card>();

    public Transform canvas;
    public CanvasGroup bgFrozen;
    public IconEffect IconEffectPre;

    [Header("ICON:")]
    public Sprite lockIcon;
    public Sprite thunderIcon;
    public Sprite fanIcon;
    public Sprite findIcon;
    public Sprite findsIcon;
    public Sprite magnetIcon;
    public Sprite frozenIcon;

    public TextMeshProUGUI txtMagnet_LevelChest;
    public TextMeshProUGUI txtFrozen_LevelChest;
    public TextMeshProUGUI txtThunder_LevelChest;

    public List<DailyReward> listDailyRewardCard = new List<DailyReward>();

    private void Awake()
    {
        if(ins == null)
            ins = this;
    }

    private void Start()
    {
        Active(home_UI);
        dailyRewardUI.SetActive(false);
        gameOver_UI.SetActive(false);
        winGame_UI.SetActive(false);
        setting_UI.SetActive(false);
        pause_UI.SetActive(false);
        mess_UI.SetActive(false);
        ResetBoostersMessUI.SetActive(false);
        selectBoostersUI.SetActive(false);
        limitedOfferUI.SetActive(false);
        levelChessUI.SetActive(false);
        giftSpeicalUI.SetActive(false);
        bgFrozen.alpha = 0;
        EventButton();
        ShowCoin();
        ShowLevel();
        SetLevelChestSlider();

       // btnDailyReward.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -15));
      //  btnDailyReward.transform.DOLocalRotate(new Vector3(0, 0, 15f), 1f).SetLoops(-1, LoopType.Yoyo);
        DateTime currentTime = DateTime.Now;
        giftCooldown_SpeialOffer = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, 23, 59, 59);
        ShowDailyRewardCard();
    }

    private void Update()
    {
        DateTime currentTime = DateTime.Now;
        TimeSpan timeRemaining2 = giftCooldown_SpeialOffer - currentTime;

        if (timeRemaining2.TotalSeconds > 0)
        {
            int hours = Mathf.FloorToInt((float)timeRemaining2.TotalHours);
            int minutes = Mathf.FloorToInt((float)timeRemaining2.Minutes);
            int seconds = Mathf.FloorToInt((float)timeRemaining2.Seconds);

            txtDailyRewardTime.text = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
        }
        else
        {
            txtDailyRewardTime.text = "CLAIM";
            if (Prefs.RECEIVED_DAILY_REWARDS == 1)
            {
                Prefs.RECEIVED_DAILY_REWARDS = 0;
            }
        }
    }

    public void ShowDailyRewardCard()
    {
        foreach (var o in listDailyRewardCard)
        {
            o.status.gameObject.SetActive(false);
            o.btn.interactable = true;
            o.enableClick = false;
        }

        for (int i = 0; i < Prefs.DAILY_REWARDS; i++)
        {
            listDailyRewardCard[i].status.gameObject.SetActive(true);
            listDailyRewardCard[i].btn.interactable = false;
        }

        if (Prefs.RECEIVED_DAILY_REWARDS == 0)
        {
            listDailyRewardCard[Prefs.DAILY_REWARDS].enableClick = true;
        }

        if (Prefs.DAILY_REWARDS == 6)
        {
            btnCollectGiftSpecial.interactable = true;
        }
        else
        {
            btnCollectGiftSpecial.interactable = false;
        }
    }

    public void ShowTextItemNbr()
    {
        txtFindNbr.text = Prefs.FIND_ITEM.ToString();
        txtFindsNbr.text = Prefs.FIND_ITEMS.ToString();
        txtMagnetNbr.text = Prefs.MAGNET_ITEM.ToString();
        txtFrozenNbr.text = Prefs.FROZEN_TIME_ITEM.ToString();
    }

    public void SetLevelChestSlider()
    {
        levelChestSlider.maxValue = 5;
        SetTextLevelChest();
        if (levelChestSlider.value >= levelChestSlider.maxValue)
        {
            DOTween.Kill(btnLevelChest.transform);
            btnLevelChest.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
        }
    }

    public void SetTextLevelChest()
    {
        levelChestSlider.value = Prefs.LEVEL_CHEST_PROGRESS;
       // txtLevelChestProgressNbr.text = Prefs.LEVEL_CHEST_PROGRESS + "/20";
    }

    public void GiftReward()
    {
        Prefs.COIN += 500;
        Prefs.FROZEN_TIME_ITEM += 2;
        Prefs.BONUS_TIME_ITEM += 2;

        ShowCoin();
    }

    public void ShowLevel()
    {
        txtLevel.text = "LEVEL " + (Prefs.LEVEL + 1);
        txtLevelInSelectBoostersUI.text = "LEVEL " + (Prefs.LEVEL + 1);
        txtLevel2.text = (Prefs.LEVEL + 1).ToString();
    }

    public void ShowCoin()
    {
        txtCoin_HomeUI.text = Prefs.COIN.ToString();
    }

    public void Active(GameObject act)
    {
        home_UI.SetActive(false);
        gamePlay_UI.SetActive(false);
        shop_UI.SetActive(false);
        commingSoonUI.SetActive(false);
        rankUI.SetActive(false);

        if (act != null)
        {
            act.SetActive(true);
        }
    }

    public void EventButton()
    {
        btnPlay.onClick.AddListener(() =>
        {
            GameEvent.ins.gameStarted.Value = true;
            ShowLevel();
            ShowTextItemNbr();
            Active(gamePlay_UI);
            menuUI.SetActive(false);
            GameManager.ins.DataLevelProcessing();
            AudioManager.ins.PlayClickSound();
        });

        btnPlay_Accept.onClick.AddListener(() =>
        {
            GameEvent.ins.gameReady.Value = true;
            selectBoostersUI.SetActive(false);
            GameManager.ins.BoostersProcessing();
            AudioManager.ins.PlayClickSound();
        });

        btnThunderItem.onClick.AddListener(() =>
        {
            if (Prefs.THUNDER_ITEM != 0)
            {
                if (GameManager.ins.isThunder)
                {
                    btnThunderItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Thunder";
                    GameManager.ins.isThunder = false;
                }
                else
                {
                    btnThunderItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Thunder\n" + "V";
                    GameManager.ins.isThunder = true;
                }
            }
            else
            {
                ManagerAds.Ins.ShowRewardedVideo((x) =>
                {
                    if (x)
                    {
                        Prefs.THUNDER_ITEM++;
                        txtThunderNbr.text = Prefs.THUNDER_ITEM.ToString();
                    }
                });
            }

            AudioManager.ins.PlayClickSound();
        });

        btnPowerfullFanItem.onClick.AddListener(() =>
        {
            if (Prefs.POWERFULL_FAN != 0)
            {
                if (GameManager.ins.isPowerfullFan)
                {
                    btnPowerfullFanItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Powerful Fan";
                    GameManager.ins.isPowerfullFan = false;
                }
                else
                {
                    btnPowerfullFanItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Powerful Fan\n" + "V";
                    GameManager.ins.isPowerfullFan = true;
                }
            }
            else
            {
                ManagerAds.Ins.ShowRewardedVideo((x) =>
                {
                    if (x)
                    {
                        Prefs.POWERFULL_FAN++;
                        txtPowerfullFanNbr.text = Prefs.POWERFULL_FAN.ToString();
                    }
                });
            }

            AudioManager.ins.PlayClickSound();
        });

        btnBonusTimeItem.onClick.AddListener(() =>
        {
            if (Prefs.BONUS_TIME_ITEM != 0)
            {
                if (GameManager.ins.isBonusTime)
                {
                    btnBonusTimeItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Bonus Time";
                    GameManager.ins.isBonusTime = false;
                }
                else
                {
                    btnBonusTimeItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Bonus Time\n" + "V";
                    GameManager.ins.isBonusTime = true;
                }
            }
            else
            {
                ManagerAds.Ins.ShowRewardedVideo((x) =>
                {
                    if (x)
                    {
                        Prefs.BONUS_TIME_ITEM++;
                        txtBonusTimeNbr.text = Prefs.BONUS_TIME_ITEM.ToString();
                    }
                });
            }
            AudioManager.ins.PlayClickSound();
        });

        btnCloseSelectBoosters.onClick.AddListener(() =>
        {
            GameEvent.ins.gameReady.Value = true;
            selectBoostersUI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnST.onClick.AddListener(() =>
        {
            GameManager.ins.ZoomInOut_DW(setting_UI.transform.GetChild(0));
            setting_UI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnST_WinUI.onClick.AddListener(() =>
        {
            GameManager.ins.ZoomInOut_DW(setting_UI.transform.GetChild(0));
            setting_UI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnST_PauseUI.onClick.AddListener(() =>
        {
            GameManager.ins.ZoomInOut_DW(setting_UI.transform.GetChild(0));
            setting_UI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnCloseST.onClick.AddListener(() =>
        {
            setting_UI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnShop.onClick.AddListener(() =>
        {
            ActiveHomeButton(btnShop);
            Active(shop_UI);
            AudioManager.ins.PlayClickSound();
        });

        btnBackHome_EndUI.onClick.AddListener(() =>
        {
            GameManager.ins.BackHome();
            AudioManager.ins.PlayClickSound();
        });

        btnHome_WinUI.onClick.AddListener(() =>
        {
            GameManager.ins.BackHome();
            AudioManager.ins.PlayClickSound();
        });

        btnSubmitQuitLevel.onClick.AddListener(() =>
        {
            Prefs.WINNING_STREAK = 0;
            GameManager.ins.LoadLevel();
            AudioManager.ins.PlayBGSound();
            ResetBoostersMessUI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnCloseResetBoostersMessUI.onClick.AddListener(() =>
        {
            Time.timeScale = 1;
            GameManager.ins.gameState = GameState.playing;
            ResetBoostersMessUI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnRetry_PauseUI.onClick.AddListener(() =>
        {
            pause_UI.SetActive(false);
            ResetBoostersMessUI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnRetry_LoseUI.onClick.AddListener(() =>
        {
            gameOver_UI.SetActive(false);
            ResetBoostersMessUI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnPause.onClick.AddListener(() =>
        {
            GameManager.ins.gameState = GameState.pause;
            GameManager.ins.ZoomInOut_DW(pause_UI.transform.GetChild(0));
            pause_UI.SetActive(true);
            Time.timeScale = 0;
            AudioManager.ins.PlayClickSound();
        });

        btnClosePauseUI.onClick.AddListener(() =>
        {
            GameManager.ins.gameState = GameState.playing;
            Time.timeScale = 1;
            pause_UI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnResumePauseUI.onClick.AddListener(() =>
        {
            GameManager.ins.gameState = GameState.playing;
            Time.timeScale = 1;
            pause_UI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnNext.onClick.AddListener(() =>
        {
            winGame_UI.SetActive(false);
            GameManager.ins.LoadLevel();
            AudioManager.ins.PlayClickSound();
            AudioManager.ins.PlayBGSound();
        });

        btnBonusCoin_HomeUI.onClick.AddListener(() =>
        {
            GameManager.ins.AddCoin(DataManager.ins.data.coinBonus_ADS);
            AudioManager.ins.PlayClickSound();
        });

        btnBonusCoin_ShopUI.onClick.AddListener(() =>
        {
            GameManager.ins.AddCoin(DataManager.ins.data.coinBonus_ADS);
            mess_UI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnClose_BonusCoin_ShopUI.onClick.AddListener(() =>
        {
            mess_UI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnFrozenTime.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayFrozenTime();
            GameManager.ins.Frozen((rs) =>
            {
                if (rs)
                {
                    IconEffect pre = Instantiate(IconEffectPre);
                    pre.SetInfo(frozenIcon, "-1", btnFrozenTime.transform.position);
                    ShowTextItemNbr();
                }
            });
            AudioManager.ins.PlayClickSound();
        });

        btnMagnet.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayMagnetSound();
            GameManager.ins.UseMagnet((rs) =>
            {
                if (rs)
                {
                    IconEffect pre = Instantiate(IconEffectPre);
                    pre.SetInfo(magnetIcon, "-1", btnMagnet.transform.position);
                    ShowTextItemNbr();  
                }
            });
            AudioManager.ins.PlayClickSound();
        });

        btnFan.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayFanSound();
            GameManager.ins.UseFan((rs) =>
            {
                if (rs)
                {
                    btnFan.transform.DORotate(new Vector3(0, 0, -360), 1f, RotateMode.FastBeyond360);
                }
            });
            AudioManager.ins.PlayClickSound();
        });


        btnFindItem.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayAutoFindSound();
            GameManager.ins.UseFindItem((rs) =>
            {
                if (rs)
                {
                    IconEffect pre = Instantiate(IconEffectPre);
                    pre.SetInfo(findIcon, "-1", btnFindItem.transform.position);
                    ShowTextItemNbr();
                }
            });
            AudioManager.ins.PlayClickSound();
        });

        btnFindItems.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayAutoFindSound();
            GameManager.ins.UseFindItems((rs) =>
            {
                if (rs)
                {
                    IconEffect pre = Instantiate(IconEffectPre);
                    pre.SetInfo(findsIcon, "-1", btnFindItems.transform.position);
                    ShowTextItemNbr();
                }
            });
            AudioManager.ins.PlayClickSound();
        });

        btnHome.onClick.AddListener(() =>
        {
            ActiveHomeButton(btnHome);
            Active(home_UI);
            AudioManager.ins.PlayClickSound();
        });

        btnLives.onClick.AddListener(() =>
        {
            ActiveHomeButton(btnLives);
            Active(commingSoonUI);
            AudioManager.ins.PlayClickSound();
        });

        btnTeam.onClick.AddListener(() =>
        {
            ActiveHomeButton(btnTeam);
            Active(commingSoonUI);
            AudioManager.ins.PlayClickSound();
        });

        btnDailyReward.onClick.AddListener(() =>
        {
            dailyRewardUI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnCloseDailyReward.onClick.AddListener(() =>
        {
            dailyRewardUI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnLimitedOffer.onClick.AddListener(() =>
        {
            limitedOfferUI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnCloseLimitedOffer.onClick.AddListener(() =>
        {
            limitedOfferUI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnCollectGiftSpecial.onClick.AddListener(() =>
        {
            Prefs.DAILY_REWARDS = 0;
            Prefs.BONUS_TIME_ITEM++;
            Prefs.POWERFULL_FAN++;
            Prefs.FIND_ITEM++;
            Prefs.FIND_ITEMS++;
            Prefs.MAGNET_ITEM++;
            Prefs.BONUS_TIME_ITEM++;
            Prefs.THUNDER_ITEM++;
            giftSpeicalUI.SetActive(true);
            AudioManager.ins.PlayClickSound();
        });

        btnCloseGiftSpecialUI.onClick.AddListener(() =>
        {
            giftSpeicalUI.SetActive(false);
            AudioManager.ins.PlayClickSound();
            ShowDailyRewardCard();
        });

        btnLevelChest.onClick.AddListener(() =>
        {
            if (levelChestSlider.value == levelChestSlider.maxValue)
            {
                Prefs.LEVEL_CHEST_RECEIVE_TIME++;
                int i = Prefs.LEVEL_CHEST_RECEIVE_TIME * 2;

                txtMagnet_LevelChest.text = i.ToString();
                txtFrozen_LevelChest.text = i.ToString();
                txtThunder_LevelChest.text = i.ToString();

                Prefs.MAGNET_ITEM += i;
                Prefs.FROZEN_TIME_ITEM += i;
                Prefs.THUNDER_ITEM += i;
                Prefs.LEVEL_CHEST_PROGRESS = 0;
                ShowTextItemNbr();
                SetTextLevelChest();
                levelChessUI.SetActive(true);
                DOTween.Kill(btnLevelChest.transform);
            }
            AudioManager.ins.PlayClickSound();
        });

        btnCloseLevelChest.onClick.AddListener(() =>
        {
            string i = (Prefs.LEVEL_CHEST_RECEIVE_TIME * 2).ToString();

            IconEffect pre = Instantiate(IconEffectPre);
            pre.SetInfo(magnetIcon, i.ToString(), btnCloseLevelChest.transform.position - new Vector3(200, -10, 0));

            IconEffect pre2 = Instantiate(IconEffectPre);
            pre2.SetInfo(frozenIcon, i, btnCloseLevelChest.transform.position + new Vector3(0,10,0)) ;

            IconEffect pre3 = Instantiate(IconEffectPre);
            pre3.SetInfo(thunderIcon, i, btnCloseLevelChest.transform.position + new Vector3(200, 10, 0));

            levelChessUI.SetActive(false);
            AudioManager.ins.PlayClickSound();
        });

        btnRank.onClick.AddListener(() =>
        {
            ActiveHomeButton(btnRank);
            Active(rankUI);
            if (Prefs.FIRST_TIME_PLAY == 1)
            {
                UserManager.Ins.ShowRank();
            }
            else
            {
                UserManager.Ins.ShowRegisterForm();
            }
            AudioManager.ins.PlayClickSound();
        });
    }

    private void ActiveHomeButton(Button btn)
    {
        btnHome.transform.localScale = Vector3.one;
        btnShop.transform.localScale = Vector3.one;
        btnRank.transform.localScale = Vector3.one;
        btnLives.transform.localScale = Vector3.one;
        btnTeam.transform.localScale = Vector3.one;

        if (btn != null)
            btn.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
    }

    public void Reward_Gift()
    {
        AudioManager.ins.PlayEatSound();
        Prefs.COIN += 500;
        ShowCoin();
        Prefs.FROZEN_TIME_ITEM += 2;
        Prefs.BONUS_TIME_ITEM += 2;
    }

    public void ShowBoostersUI()
    {
       /* btnThunderItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Thunder";
        btnPowerfullFanItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Powerful Fan";
        btnBonusTimeItem.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Bonus Time";*/

        txtBonusTimeNbr.text = Prefs.BONUS_TIME_ITEM == 0 ? "+" : Prefs.BONUS_TIME_ITEM.ToString();
        txtThunderNbr.text = Prefs.THUNDER_ITEM == 0 ? "+" : Prefs.THUNDER_ITEM.ToString();
        txtPowerfullFanNbr.text = Prefs.POWERFULL_FAN == 0 ? "+" : Prefs.POWERFULL_FAN.ToString();

        levelRewardProgression.maxValue = 3;
        levelRewardProgression.value = Prefs.WINNING_STREAK;

        Debug.Log("pROGRESS" + Prefs.WINNING_STREAK);

        selectBoostersUI.transform.GetChild(0).localScale = Vector3.zero;
        selectBoostersUI.transform.GetChild(0).DOScale(Vector3.one, .5f);
        selectBoostersUI.SetActive(true);
    }

    public void RenderFindCard()
    {
        foreach (var c in DataManager.ins.data.dataLevels.listLevel[GameManager.ins.levelIdx].nodeFindList)
        {
            Transform pre = Instantiate(cardPre, findCardList.position, findCardList.rotation);
            pre.DOLocalRotate(new Vector3(0, 180, 0), 1f).OnComplete(() =>
            {
                pre.DOLocalRotate(new Vector3(0, -360, 0), 1f);
            });
            CardInfo card = DataManager.ins.data.dataCard.listCard.Find(c2 => c2.id == c.id);
            if (card != null)
            {
                pre.GetComponent<Card>().Setter(c.id, card.icon, c.amount);
                pre.SetParent(findCardList);
                pre.localScale = Vector3.one;
                listFindCard.Add(pre.GetComponent<Card>());
            }
        }
    }

    public void ClearFindCard()
    {
        foreach (var c in listFindCard)
        {
            Destroy(c.gameObject);
        }

        listFindCard.Clear();
    }
}
