using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "New Data")]
public class Data : ScriptableObject
{
    public DataLevels dataLevels;
    public DataCard dataCard;
    [Space]
    [Header("COST:")]
    public int coinBonus_ADS;
    public int magnetItemCost;
    public int frozenTimeItemCost;
    public int changeItemCost;
    public int bonusTimeItemCost;
    public int largePackCost;
}
