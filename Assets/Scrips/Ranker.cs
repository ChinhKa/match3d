using TMPro;
using UnityEngine;

public class Ranker : MonoBehaviour
{
    public GameObject icon;
    public TextMeshProUGUI rank;
    public TextMeshProUGUI name;
    public TextMeshProUGUI scores;

    public void SetInfo(string rank, string name, string scores)
    {
        this.rank.text = rank;
        this.name.text = name;
        this.scores.text = scores;
    }

    public void ShowRankFrame(bool isOn)
    {
        if (isOn)
        {
            icon.SetActive(true);
        }
        else
        {
            icon.SetActive(false);
        }
    }
}
