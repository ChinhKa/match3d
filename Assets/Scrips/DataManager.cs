﻿using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager ins;
    public Data data;

    private void Awake()
    {
        if (ins == null)
            ins = this;
    }

    private void Start()
    {
        if (!PlayerPrefs.HasKey(GameConstants.COIN))
        {
            Prefs.COIN = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.LEVEL))
        {
            Prefs.LEVEL = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.BONUS_TIME_ITEM))
        {
            Prefs.BONUS_TIME_ITEM = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.THUNDER_ITEM))
        {
            Prefs.THUNDER_ITEM = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.POWERFULL_FAN))
        {
            Prefs.POWERFULL_FAN = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.FROZEN_TIME_ITEM))
        {
            Prefs.FROZEN_TIME_ITEM = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.MAGNET_ITEM))
        {
            Prefs.MAGNET_ITEM = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.FIND_ITEM))
        {
            Prefs.FIND_ITEM = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.FIND_ITEMS))
        {
            Prefs.FIND_ITEMS = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.LEVEL_CHEST_PROGRESS))
        {
            Prefs.LEVEL_CHEST_PROGRESS = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.WINNING_STREAK))
        {
            Prefs.WINNING_STREAK = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.DAILY_REWARDS))
        {
            Prefs.DAILY_REWARDS = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.LEVEL_CHEST_RECEIVE_TIME))
        {
            Prefs.LEVEL_CHEST_RECEIVE_TIME = 0;
        }
        if (!PlayerPrefs.HasKey(GameConstants.RECEIVED_DAILY_REWARDS))
        {
            Prefs.RECEIVED_DAILY_REWARDS = 0;
        }
        LoadData();
    }

    private void LoadData()
    {
        UIManager.ins.fanSlider.maxValue = 20;
        UIManager.ins.fanSlider.value = 0;
    }
}