using UnityEngine;

public class Prefs
{
    public static int COIN
    {
        get => PlayerPrefs.GetInt(GameConstants.COIN);
        set
        {
            PlayerPrefs.SetInt(GameConstants.COIN, value);
        }
    }

    public static int LEVEL
    {
        get => PlayerPrefs.GetInt(GameConstants.LEVEL);
        set
        {
            PlayerPrefs.SetInt(GameConstants.LEVEL, value);
        }
    }

    public static int MAGNET_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.MAGNET_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.MAGNET_ITEM, value);
        }
    }

    public static int FROZEN_TIME_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.FROZEN_TIME_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.FROZEN_TIME_ITEM, value);
        }
    }

    public static int FIND_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.FIND_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.FIND_ITEM, value);
        }
    }

    public static int FIND_ITEMS
    {
        get => PlayerPrefs.GetInt(GameConstants.FIND_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.FIND_ITEM, value);
        }
    }

    public static int THUNDER_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.THUNDER_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.THUNDER_ITEM, value);
        }
    }

    public static int POWERFULL_FAN
    {
        get => PlayerPrefs.GetInt(GameConstants.POWERFULL_FAN);
        set
        {
            PlayerPrefs.SetInt(GameConstants.POWERFULL_FAN, value);
        }
    }

    public static int BONUS_TIME_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.BONUS_TIME_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.BONUS_TIME_ITEM, value);
        }
    }

    public static int BG_MUSIC
    {
        get => PlayerPrefs.GetInt(GameConstants.BG_MUSIC);
        set
        {
            PlayerPrefs.SetInt(GameConstants.BG_MUSIC, value);
        }
    }

    public static int VFX_MUSIC
    {
        get => PlayerPrefs.GetInt(GameConstants.VFX_MUSIC);
        set
        {
            PlayerPrefs.SetInt(GameConstants.VFX_MUSIC, value);
        }
    }

    public static int NOTIFY
    {
        get => PlayerPrefs.GetInt(GameConstants.NOTIFY);
        set
        {
            PlayerPrefs.SetInt(GameConstants.NOTIFY, value);
        }
    }

    public static float LEVEL_CHEST_PROGRESS
    {
        get => PlayerPrefs.GetFloat(GameConstants.LEVEL_CHEST_PROGRESS);
        set
        {
            PlayerPrefs.SetFloat(GameConstants.LEVEL_CHEST_PROGRESS, value);
        }
    }

    public static int PLAY_DATE
    {
        get => PlayerPrefs.GetInt(GameConstants.PLAY_DATE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.PLAY_DATE, value);
        }
    }

    public static int WINNING_STREAK
    {
        get => PlayerPrefs.GetInt(GameConstants.WINNING_STREAK);
        set
        {
            PlayerPrefs.SetInt(GameConstants.WINNING_STREAK, value);
        }
    }

    public static int DAILY_REWARDS
    {
        get => PlayerPrefs.GetInt(GameConstants.DAILY_REWARDS);
        set
        {
            PlayerPrefs.SetInt(GameConstants.DAILY_REWARDS, value);
        }
    }

    public static int LEVEL_CHEST_RECEIVE_TIME
    {
        get => PlayerPrefs.GetInt(GameConstants.LEVEL_CHEST_RECEIVE_TIME);
        set
        {
            PlayerPrefs.SetInt(GameConstants.LEVEL_CHEST_RECEIVE_TIME, value);
        }
    }

    public static string START_TIME
    {
        get => PlayerPrefs.GetString(GameConstants.START_TIME);
        set
        {
            PlayerPrefs.SetString(GameConstants.START_TIME, value);
        }
    }

    public static string USER_NAME
    {
        get => PlayerPrefs.GetString(GameConstants.USER_NAME);
        set
        {
            PlayerPrefs.SetString(GameConstants.USER_NAME, value);
        }
    }

    public static int FIRST_TIME_PLAY
    {
        get => PlayerPrefs.GetInt(GameConstants.FIRST_TIME_PLAY);
        set
        {
            PlayerPrefs.SetInt(GameConstants.FIRST_TIME_PLAY, value);
        }
    }

    public static int RECEIVED_DAILY_REWARDS
    {
        get => PlayerPrefs.GetInt(GameConstants.RECEIVED_DAILY_REWARDS);
        set
        {
            PlayerPrefs.SetInt(GameConstants.RECEIVED_DAILY_REWARDS, value);
        }
    }
}
