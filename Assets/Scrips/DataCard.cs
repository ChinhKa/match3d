using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data Card", menuName = "New Data Card")]
public class DataCard : ScriptableObject
{
    public List<CardInfo> listCard = new List<CardInfo>();
}

[System.Serializable]
public class CardInfo
{
    public CardID id;
    public Transform pre;
    public Sprite icon;
}
